import React, {Component} from 'react'
import SearchBar from '../../Components/SearchBar/SearchBar'
import DataProvider from '../../Data/DataProvider'
import Books from '../../Components/Books/Books'

class BookSearch extends Component {
    state = {
        books: [],
        error: false,
        loading: true,
        searchText: null
    };

    componentDidMount() {
        this.setState({books: DataProvider})
    }

    searchHandler = () => {

    };

    searchTextChangeHandler = (event) => {
        const newText = event.target.value;
        console.log("Test");
        this.setState({text: newText})
    };


    render() {
        return (
            <div>
                <SearchBar searchTextChangeHandler={this.searchTextChangeHandler} />
                <Books books={this.state.books} searchHandler={this.searchHandler}/>
            </div>
        );
    }
}

export default BookSearch