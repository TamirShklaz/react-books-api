import React from 'react'
import Book from './Book/Book'

const books = (props) => {

    let bookList = [];
    bookList = props.books.map(book => {

        return <Book key={book.ISBN} name={book.Name} author={book.Author} ISBN={book.ISBN}/>
    });


    return (
        <div>
            {bookList}
        </div>
    );

};

export default books;