import React, {Component} from 'react'

class SearchBar extends Component {
    state = {
        text: null

    };


    render() {
        return (
            <div>
                <input type="text" onChange={this.props.searchTextChangeHandler} name="book-name" value={this.props.searchText}/>
                <button onClick={this.props.searchHandler}>Search</button>
            </div>
        );
    }
};

export default SearchBar