import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Book from './Components/Books/Book/Book'
import Books from './Components/Books/Books'
import BookSearch from './Containers/BookSearch/BookSearch'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

class App extends Component {

    state = {
        books: [{name: "Name", author: "Author", ISBN: "ISBN"},
            {name: "Name1", author: "Author", ISBN: "ISBN"},
            {name: "Name", author: "Author", ISBN: "ISBN"}]
    };


    render() {
        return (
            <MuiThemeProvider>
                <BookSearch/>
            </MuiThemeProvider>


        );
    }
}

export default App;
